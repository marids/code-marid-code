/*-
 * #%L
 * marid-ide
 * %%
 * Copyright (C) 2012 - 2017 MARID software development group
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.marid.dependant.beaneditor;

import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;
import org.marid.annotation.MetaLiteral;
import org.marid.dependant.beaneditor.dao.LibraryBeanDao;
import org.marid.dependant.beaneditor.model.LibraryBean;
import org.marid.dependant.beaneditor.model.LibraryMethod;
import org.marid.dependant.beaneditor.model.WildBean;
import org.marid.ide.model.BeanData;
import org.marid.ide.model.BeanMethodData;
import org.marid.ide.project.ProjectProfile;
import org.marid.jfx.action.FxAction;
import org.marid.jfx.action.SpecialAction;
import org.marid.jfx.dnd.DndManager;
import org.marid.jfx.track.PeriodicObservable;
import org.marid.runtime.context.MaridRuntimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static java.util.logging.Level.WARNING;
import static java.util.stream.Collectors.*;
import static org.marid.ide.IdeNotifications.n;
import static org.marid.ide.model.ClipboardUtils.*;
import static org.marid.jfx.LocalizedStrings.ls;
import static org.marid.l10n.L10n.m;
import static org.marid.l10n.L10n.s;

/**
 * @author Dmitry Ovchinnikov
 */
@Component
public class BeanTableActions {

    private final DndManager dndManager;

    @Autowired
    public BeanTableActions(DndManager dndManager) {
        this.dndManager = dndManager;
    }

    @Bean
    public BeanTableAction cutBeanAction(SpecialAction cutAction) {
        return data -> {
            if (data.parent == null) {
                return null;
            }
            return new FxAction("ccv", cutAction)
                    .setEventHandler(event -> {
                        save(data.toBean(), dndManager.clipboard());
                        data.parent.children.remove(data);
                    });
        };
    }

    @Bean
    public BeanTableAction pasteBeanAction(SpecialAction pasteAction, PeriodicObservable bySeconds) {
        return data -> new FxAction("ccv", pasteAction)
                .bindDisabled(bySeconds.b(() -> !hasBeanData(dndManager.clipboard())))
                .setEventHandler(event -> load(dndManager.clipboard()).ifPresent(bean -> {
                    final BeanData beanData = new BeanData(data, bean);
                    data.children.add(beanData);
                }));
    }

    @Bean
    public BeanTableAction copyBeanAction(SpecialAction copyAction) {
        return data -> {
            if (data.parent == null) {
                return null;
            }
            return new FxAction("ccv", copyAction)
                    .setIcon("D_CONTENT_COPY")
                    .setEventHandler(event -> save(data.toBean(), dndManager.clipboard()));
        };
    }

    @Bean
    public BeanTableAction addRootBeanAction(SpecialAction addAction, LibraryBeanDao dao, ProjectProfile profile) {
        return data -> {
            final FxAction action = new FxAction("add", addAction)
                    .bindText("Add a bean")
                    .setIcon("D_SERVER_PLUS");
            final Consumer<ProjectProfile> listener = p -> {
                final Function<LibraryBean, FxAction> function = bean -> new FxAction("bean", bean.literal.group)
                        .setIcon(bean.literal.icon)
                        .bindText(ls("%s%s%s", lo(bean.literal)))
                        .setEventHandler(event -> data.add(bean.bean));
                final Map<String, List<FxAction>> grouped = dao.beans()
                        .collect(groupingBy(b -> b.literal.group, TreeMap::new, mapping(function, toList())));
                action.setChildren(children(grouped));
            };
            action.anchors.add(listener);
            listener.accept(profile);
            profile.addOnUpdate(listener);
            return action;
        };
    }

    @Bean
    public BeanTableAction factoryBeansC(SpecialAction addAction, LibraryBeanDao dao, ProjectProfile profile) {
        return data -> {
            final FxAction action = new FxAction("add", addAction)
                    .bindText("Add a factory bean (as a child)")
                    .setIcon("D_SERVER_PLUS");
            final Consumer<ProjectProfile> listener = p -> {
                final Function<LibraryBean, FxAction> function = bean -> new FxAction("bean", bean.literal.group)
                        .setIcon(bean.literal.icon)
                        .bindText(ls("%s%s%s", lo(bean.literal)))
                        .setEventHandler(event -> {
                            final BeanData beanData = data.add(bean.bean);
                            beanData.factory.set(data.getName());
                        });
                final Map<String, List<FxAction>> grouped = dao.beans(data)
                        .collect(groupingBy(b -> b.literal.group, TreeMap::new, mapping(function, toList())));
                action.setChildren(children(grouped));
            };
            action.anchors.add(listener);
            listener.accept(profile);
            profile.addOnUpdate(listener);
            return action;
        };
    }

    @Bean
    public BeanTableAction factoryBeansS(SpecialAction addAction, LibraryBeanDao dao, ProjectProfile profile) {
        return data -> {
            final FxAction action = new FxAction("add", addAction)
                    .bindText("Add a factory bean (at the same level)")
                    .setIcon("D_SERVER_PLUS");
            final Consumer<ProjectProfile> listener = p -> {
                final Function<LibraryBean, FxAction> function = bean -> new FxAction("bean", bean.literal.group)
                        .setIcon(bean.literal.icon)
                        .bindText(ls("%s%s%s", lo(bean.literal)))
                        .setDisabled(data.parent == null)
                        .setEventHandler(event -> {
                            final BeanData parent = requireNonNull(data.parent);
                            final int index = parent.children.indexOf(data);
                            final BeanData beanData = new BeanData(parent, bean.bean);
                            beanData.factory.set(data.getName());
                            parent.children.add(index + 1, beanData);
                        });
                final Map<String, List<FxAction>> grouped = dao.beans(data)
                        .collect(groupingBy(b -> b.literal.group, TreeMap::new, mapping(function, toList())));
                action.setChildren(children(grouped));
            };
            action.anchors.add(listener);
            listener.accept(profile);
            profile.addOnUpdate(listener);
            return action;
        };
    }

    @Bean
    public BeanTableAction initializerAdder(ProjectProfile profile, LibraryBeanDao dao, SpecialAction addAction) {
        return data -> {
            final FxAction action = new FxAction("add", addAction)
                    .bindText("Add an initializer")
                    .setIcon("D_PLUS");
            final Consumer<ProjectProfile> listener = p -> {
                final Function<LibraryMethod, FxAction> function = m -> new FxAction("bean", m.literal.group)
                        .setIcon(m.literal.icon)
                        .bindText(MaridRuntimeUtils.toCanonical(m.method.signature))
                        .setEventHandler(event -> {
                            final BeanMethodData d = new BeanMethodData(data, m.method);
                            data.initializers.add(d);
                        });
                final Map<String, List<FxAction>> grouped = dao.initializers(data)
                        .collect(groupingBy(b -> b.literal.group, TreeMap::new, mapping(function, toList())));
                action.setChildren(children(grouped));
            };
            action.anchors.add(listener);
            listener.accept(profile);
            profile.addOnUpdate(listener);
            return action;
        };
    }

    @Bean
    public BeanTableAction wildBeanAdder(LibraryBeanDao dao, SpecialAction addAction) {
        return data -> new FxAction("add", addAction)
                .bindText("Add a wild bean")
                .setIcon("D_SERVER_NETWORK")
                .setEventHandler(event -> {
                    final TextInputDialog dialog = new TextInputDialog();
                    dialog.setHeaderText(m("Enter a class name:"));
                    dialog.setTitle(s("Wild bean"));
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    dialog.showAndWait().ifPresent(type -> {
                        final WildBean[] beans = dao.beans(type).map(WildBean::new).toArray(WildBean[]::new);
                        if (beans.length == 0) {
                            n(WARNING, "No beans found");
                        }
                        final ChoiceDialog<WildBean> d = new ChoiceDialog<>(beans[0], beans);
                        d.setTitle(s("Bean selector"));
                        d.setHeaderText(m("Select a bean: "));
                        d.initModality(Modality.APPLICATION_MODAL);
                        d.showAndWait().ifPresent(bean -> data.add(bean.bean));
                    });
                });
    }

    public static Object[] lo(MetaLiteral literal) {
        if (literal.description.isEmpty()) {
            return new Object[]{ls(literal.name), "", ""};
        } else {
            return new Object[]{ls(literal.name), ": ", ls(literal.description)};
        }
    }

    private List<FxAction> children(Map<String, List<FxAction>> grouped) {
        switch (grouped.size()) {
            case 1:
                return grouped.values().stream().flatMap(Collection::stream).collect(toList());
            default:
                return grouped.entrySet().stream().map(e -> new FxAction("", "")
                        .bindText(e.getKey())
                        .setChildren(e.getValue())).collect(Collectors.toList());
        }
    }
}
