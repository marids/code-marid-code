/*-
 * #%L
 * marid-ide
 * %%
 * Copyright (C) 2012 - 2017 MARID software development group
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.marid.ide.logging;

import com.google.common.util.concurrent.AtomicDouble;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.control.Separator;
import javafx.scene.layout.BorderPane;
import org.marid.jfx.logging.LogComponent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * @author Dmitry Ovchinnikov.
 * @since 0.9
 */
@Component
public class IdeLogPane extends BorderPane {

    private final Separator divider;
    private final LogComponent component = new LogComponent(IdeLogHandler.LOG_RECORDS);

    public IdeLogPane() {
        setTop(divider = new Separator(Orientation.HORIZONTAL));
        setMinHeight(100);
        setPrefHeight(250);

        final AtomicDouble lastPos = new AtomicDouble();
        final AtomicDouble lastHeight = new AtomicDouble();
        divider.setOnMouseEntered(event -> {
            divider.getScene().setCursor(Cursor.V_RESIZE);
            lastPos.set(event.getScreenY());
            lastHeight.set(getHeight());
        });
        divider.setOnMouseExited(event -> divider.getScene().setCursor(Cursor.DEFAULT));
        divider.setOnMouseDragged(event -> {
            final double delta = event.getScreenY() - lastPos.get();
            setPrefHeight(min(getMaxHeight(), max(getMinHeight(), lastHeight.get() - delta)));
            getParent().requestLayout();
        });
    }

    @EventListener
    public void onContextStart(ContextStartedEvent event) {
        setCenter(component);
        component.scrollTo(component.getItems().size() - 1);
    }
}
