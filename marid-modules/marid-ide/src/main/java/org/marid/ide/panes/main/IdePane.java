/*-
 * #%L
 * marid-ide
 * %%
 * Copyright (C) 2012 - 2017 MARID software development group
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package org.marid.ide.panes.main;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.marid.ide.status.IdeStatusBar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @author Dmitry Ovchinnikov
 */
@Component
@Lazy(false)
public class IdePane extends BorderPane {

    @Autowired
    private void center(IdeMainPane pane) {
        setCenter(pane);
    }

    @Autowired
    private void top(IdeMenu ideMenu, IdeToolbar toolbar) {
        final BorderPane menuPane = new BorderPane();
        menuPane.setCenter(ideMenu);
        setTop(new VBox(menuPane, toolbar));
    }

    @Autowired
    private void bottom(IdeStatusBar statusBar) {
        setBottom(statusBar);
    }
}
